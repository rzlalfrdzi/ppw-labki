$(document).ready(function() {

    $("form").on("submit", function( event ) {
      event.preventDefault();
      var data = $(this).serializeArray().reduce(function(obj, item) {
        obj[item.name] = item.value
        return obj
      }, {});
  
      console.log(data);
      var csrf_token = data['csrfmiddlewaretoken'];
      var name = data['name'];
      var email = data['email'];
      var password = data['password'];
  
      $.ajax({
        url: "api/register",
        type: "POST",
        data:  {
          csrfmiddlewaretoken: data.csrfmiddlewaretoken,
          name: data.name,
          email: data.email,
          password: data.password,
        },
        beforeSend: function(xhr, settings) {
          if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", data.csrfmiddlewaretoken)
          }
        },
        success: function(result){
          if (result.success == true) {
            $("form input").each(function(){
              $(this).removeClass("is-valid");
              $(this).val('');
            })
            alert("You have successfully registered")
          } else {
            alert("Oops something went wrong")
          }
        }
      });
    });
  
    $("form").change(function() {
      if (validateAll()) {
        console.log("validate all success")
        $("#register-button").prop('disabled', false)
      } else {
        $("#register-button").prop('disabled', true)
      }
    });
  
    $("form input[name='name']").change(function() {
      validateName($("form input[name='name']").val(), true);
    });
  
    $("form input[name='email']").change(function() {
      validateEmail($("form input[name='email']").val(), true);
    });
  
    $("form input[name='password']").change(function() {
      validatePassword($("form input[name='password']").val(), true);
    });
  
    function validateName(name, toggleValid) {
      if (name !== '') {
        if (toggleValid) {
          $("form input[name='name']").removeClass("is-invalid");
          $("form input[name='name']").addClass("is-valid");
        }
        return true;
      } else {
        if (toggleValid) {
          $("form input[name='name']").removeClass("is-valid");
          $("form input[name='name']").addClass("is-invalid");
        }
        return false;
      }
    }
  
    function validateEmail(email, toggleValid) {
      var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
      var emailIsUnique = false;
      return $.when((checkUniqueEmail(email)).then(function(json){
        if (json.valid) emailIsUnique = true;
        if(email_regex.test(email) && emailIsUnique) { 
          if (toggleValid) {
            $("form input[name='email']").removeClass("is-invalid");
            $("form input[name='email']").addClass("is-valid");
          }
          return true;  
        } else {
          // console.log("invalid email")
          if (toggleValid) {
            $("form input[name='email']").removeClass("is-valid");
            $("form input[name='email']").addClass("is-invalid");
          }
          return false;
        }
      }));
    }
  
    function validatePassword(password, toggleValid) {
      if (password !== '') {
        if (toggleValid) {
          $("form input[name='password']").removeClass("is-invalid");
          $("form input[name='password']").addClass("is-valid");
        }
        return true;
      } else {
        if (toggleValid) {
          $("form input[name='password']").removeClass("is-valid");
          $("form input[name='password']").addClass("is-invalid");
        }
        return false;
      }
    }
  
    function validateAll() {
      var data = $("form").serializeArray();
      if (validateName(data[1]['value'], false) && validateEmail(data[2]['value'], false) && validatePassword(data[3]['value'], false)) {
        return true;
      }
      return false;
    }
  
    function checkUniqueEmail(email) {
      return $.ajax({
        url: "api/validate_email?q=" + email,
        success: function(result) {
          return result;
        }
      });
    }
    
    function csrfSafeMethod(method) {
      // these HTTP methods do not require CSRF protection
      return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
  });
  