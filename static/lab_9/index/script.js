$(document).ready(function() {

    // $(".fas-star").each(function(index) {
    // $(".favourite-icon").each(function(index) {
      // $(this).click(function() {
    // $(".fa-star").click(function() {
    //   if (sessionStorage.favouriteCount) {
    //     sessionStorage.favouriteCount = Number(sessionStorage.favouriteCount) + 1;
    //   } else {
    //     sessionStorage.favouriteCount = 1;
    //   }
    //   $("#session-counter").html(sessionStorage.favouriteCount);
    //   console.log("counts");
    //   console.log("count " + sessionStorage.favouriteCount);
    // });
    // });
  
    // self invoking function to get json data
    (function() {
      $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=quilting",
        success: function (result) {
          // set to only read 10 books
          var book_count = 10;
          if (result["items"].length < 10) {
            book_count = result["items"].length
          }
  
          // add detail of each book to table
          var books_json = result["items"];
          var tmp = "";
          for(var i=0; i<book_count; i++) {
            var img_link = result["items"][i]["volumeInfo"]["imageLinks"]["smallThumbnail"];
            var title = result["items"][i]["volumeInfo"]["title"];
            var description = result["items"][i]["volumeInfo"]["description"];
            if (description == null || description == undefined) {
              description = "no description available";
            }else {
              description = description.slice(0,150) + '...';
            }
  
            tmp += '<tr>' +
                    '<td><img src="'+img_link+'"></td>' +
                    '<td>'+title+'</td>' +
                    '<td>'+description+'</td>' +
                    '<td><span class="favourite-icon"><i class="fas fa-star"></i></span></td>' +
                   '</tr>';
          }
          $("#books tbody").html("");
          $("#books tbody").html(tmp);
  
          setCounter(book_count);
        }
      });
    })(); // end of self invoking function
  
    $("#submit-button").click(function() {
      // set loading icon before ajax request
      $("#books tbody").html("");
      $("#books tbody").html('<tr><td colspan="3"><div class="loader"></div></td></tr>');
      $("#counter").text("-");
  
      // get input value
      var searchQuery = $("#search-query").val();
  
      $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + searchQuery,
        success: function (result) {
          // empty books table
          var book_count = 10;
  
          // prevent error when query does not match any books
          if (result["totalItems"] == 0) {
            $("#books tbody").html("<tr><td colspan='3' style='text-align: center'>no books found :(</td></tr>")
          }
  
          // set to only read 10 books
          if (result["items"].length < 10) {
            book_count = result["items"].length
          }
  
          // fill books table with new data
          var tmp = ""
          for(var i=0; i<book_count; i++) {
            var img_link
            if (result["items"][i]["volumeInfo"].hasOwnProperty("imageLinks") && result["items"][i]["volumeInfo"]["imageLinks"].hasOwnProperty("smallThumbnail")) {
              img_link = result["items"][i]["volumeInfo"]["imageLinks"]["smallThumbnail"];
            } else {
              img_link="static/lab_9/noimage.jpg";
            }
            var title = result["items"][i]["volumeInfo"]["title"];
            var description = result["items"][i]["volumeInfo"]["description"];
            if (description == null || description == undefined) {
              description = "no description available";
            }else {
              description = description.slice(0,150) + '...';
            }
            
            tmp += '<tr>' +
                    '<td><img src="'+img_link+'"></td>' +
                    '<td>'+title+'</td>' +
                    '<td>'+description+'</td>' +
                    '<td><span class="favourite-icon"><i class="fas fa-star"></i></span></td>' +
                   '</tr>';
          }
          $("#books tbody").html("");
          $("#books tbody").html(tmp);
  
          setCounter(book_count);
        }
      });
    })
  
    function setCounter(book_count) {
      // counter to keep track how many favourite books
      var counter = 0;
      $("#counter").text(counter + " out of " + book_count);
      // add click event listener to each favourite icon
      $('.favourite-icon').each(function(index) {
        $(this).click(function() {
          if($(this).hasClass("active")) {
            $(this).removeClass("active");
            counter--;
            updateSessionCounter("decrement");
            $("#counter").text(counter + " out of " + book_count);
          } else {
            $(this).addClass("active");
            counter++;
            updateSessionCounter("increment");
            console.log("updated")
            $("#counter").text(counter + " out of " + book_count);
          }
        })
      })
    }
  
    // function to update session counter
    function updateSessionCounter(mode) {
      $.ajax({
        url: "api/update_counter?q=" + mode,
        type: "GET",
        success: function(result) {
          if (result.success == true) {
            $("#session-counter").text(result.counter);
            console.log("suce ajax");
          } else {
            $("#session-counter").text("-");
            console.log("fail above ajax");
          }
        },
        fail: function() {
          console.log("fail ajax");
        }
      })
    }
  
    // self invoking function to display current session counter on page load
    (function () {
      $.ajax({
        url: "api/update_counter",
        type: "GET",
        success: function(result) {
          if (result.success == true) {
            $("#session-counter").text(result.counter);
          } else {
            $("#session-counter").text("-");
          }
        }
      })
    })();
  });
  