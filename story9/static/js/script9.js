$(document).ready(function() {

	$.ajax({
		url: "https://www.googleapis.com/books/v1/volumes?q=quilting",
		datatype: 'json',
		success: function (result) {
			console.log()
			$(".book-item").remove();
			for (let i = 0; i < result.items.length; i++) {
				// noinspection JSUnresolvedVariable
				$('.book-list').append(
			"<tr class='book-item'><th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
			"<td class='align-middle'>" + result.items[i].volumeInfo.title +"</td>" +
			"<td class='align-middle'>" + result.items[i].volumeInfo.authors + "</td>" + 
			"<td class='align-middle'>" + result.items[i].volumeInfo.publisher +"</td>" + 
			"<td class='align-middle'>" + result.items[i].volumeInfo.publishedDate +"</td>" + 
			"<td><img class='img-fluid' style='width:22vh' src='" + result.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
			"<td class='align-middle' style='text-align:center'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.ibb.co/nymtX0/sebelum.png'>" + "</td></tr>"
				
				)
			}
		}
	})
	// $.ajax({
	// 	url: "data",
	// 	datatype: 'json',
	// 	success: function(data){
	// 		var result ="";
	// 		for(var i = 0; i < data.items.length; i++) {
	// 			result += 
	// 			"<tr><th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
	// 			"<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
	// 			"<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" + 
	// 			"<td class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" + 
	// 			"<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" + 
	// 			"<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
	// 			"<td class='align-middle' style='text-align:center'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.ibb.co/nymtX0/sebelum.png'>" + "</td></tr>";
	// 		}
	// 		$('tbody').append(result);
	// 	}
	// })

	$("#submit-button").click(function () {
		console.log("LOL")
		let term = $("#search-query").val();
		console.log(term)
        $.ajax({
			url: "https://www.googleapis.com/books/v1/volumes?q=" + term,
			datatype: 'json',
            success: function (result) {
				console.log()
                $(".book-item").remove();
                for (let i = 0; i < result.items.length; i++) {
                    // noinspection JSUnresolvedVariable
                    $('.book-list').append(
				"<tr class='book-item'><th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
				"<td class='align-middle'>" + result.items[i].volumeInfo.title +"</td>" +
				"<td class='align-middle'>" + result.items[i].volumeInfo.authors + "</td>" + 
				"<td class='align-middle'>" + result.items[i].volumeInfo.publisher +"</td>" + 
				"<td class='align-middle'>" + result.items[i].volumeInfo.publishedDate +"</td>" + 
				"<td><img class='img-fluid' style='width:22vh' src='" + result.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
				"<td class='align-middle' style='text-align:center'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.ibb.co/nymtX0/sebelum.png'>" + "</td></tr>"
					
					)
				}
			}
		})
		$(".favorite-button").click(function () {
			if (!$(this).hasClass("selected")) {
				counter++;
				$("#counter").html(counter.toString());
				$(this).toggleClass("selected");
				$(this).toggleClass("btn-outline-secondary");
				$(this).toggleClass("btn-secondary");
				sessionStorage.setItem("favorite", counter.toString());
			} else {
				counter--;
				$("#counter").html(counter.toString());
				$(this).toggleClass("selected");
				$(this).toggleClass("btn-outline-secondary");
				$(this).toggleClass("btn-secondary");
				sessionStorage.setItem("favorite", counter.toString());
			}
		});
	})
});




var counter = 0;
function favorite(clicked_id){
	var button = document.getElementById(clicked_id);
	if(button.classList.contains("checked")){
		button.classList.remove("checked");
		document.getElementById(clicked_id).src = 'https://image.ibb.co/nymtX0/sebelum.png';
		counter--;
		document.getElementById("counter").innerHTML = counter;
	}

	else{
		button.classList.add('checked');
		document.getElementById(clicked_id).src = 'https://image.ibb.co/dLALC0/sesudah.png';
		counter++;
		document.getElementById("counter").innerHTML = counter;ß
	}
}




