from django.shortcuts import render
from .forms import Forms
from .models import Schedule
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.
response = {}
def index1(request):
    return render(request, 'middle.html', response)

def register(request):
    return render(request, 'register.html')

def about(request):
    return render(request, 'aboutus.html')

def contacts(request):
    return render(request, 'contacts.html')

def education(request):
    return render(request, 'education.html')

def scheduling(request):
	response = {'schedule_form' : Forms	}
	return render(request, "calendar.html", response)

def add_schedule(request):
	forms = Forms(request.POST)
	if(request.method == 'POST'):
		if(forms.is_valid()):
			clean = forms.cleaned_data
			schedule = Schedule()
			schedule.activity = clean['activity']
			schedule.datetime = clean['datetime']
			schedule.locations = clean['locations']
			schedule.category = clean['category']
			schedule.time = clean['time']
			schedule.save()
			return HttpResponseRedirect(reverse('story4:show'))

def show(request):
	response = {
	'schedule' : Schedule.objects.all()
	}
	return render(request,"showschedule.html",response)

def erase(request):
	Schedule.objects.all().delete()
	return HttpResponseRedirect(reverse('story4:show'))
