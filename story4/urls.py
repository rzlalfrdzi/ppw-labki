from django.conf.urls import url
from .views import index1
from .views import register
from .views import about
from .views import contacts
from .views import education
from .views import scheduling
from .views import add_schedule
from .views import show
from .views import erase

app_name = 'story4'

#url for app
urlpatterns = [
    url(r'^$', index1, name='index1'),
    url(r'register/$', register, name='register'),
    url(r'about/$', about , name='about' ),
    url(r'contacts/$', contacts, name='contacts' ),

    url(r'education/$', education, name='education'),
    url(r'scheduling/$', scheduling, name='scheduling'),
    url(r'add_schedule/$', add_schedule, name='add_schedule'),
    url(r'show/$', show, name='show'),
    url(r'erase/$', erase, name='erase')

]


