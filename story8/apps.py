# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class Story8Config(AppConfig):
    name = 'story8'
