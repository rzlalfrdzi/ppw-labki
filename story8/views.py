# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
def lab8(request):

    response = {}
    return render(request, 'test.html', response)