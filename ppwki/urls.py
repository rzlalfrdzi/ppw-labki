"""ppwki URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
from django.urls import path, include, re_path


import story4.urls as story4
import story6.urls as story6
import story9.urls as story9
import story8.urls as story8


from story8.views import *
from story10.views import *
from story9.views import *
from django.conf import settings
from django.conf.urls.static import static




urlpatterns = [
    url('admin/', admin.site.urls),
    url(r'^', include(story4, namespace="story4")),


    re_path(r'^auth/', include('social_django.urls', namespace='social')),

    # path('lab8/', include('story8.urls')),
    
    path('lab9/', include('story9.urls')),
    path('lab10/', include('story10.urls')),

    

    re_path(r'^lab10$', lab10, name='lab10'),
    path('valid', valid, name="valid"),
    path('post', post, name="post"),

    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    url(r'^auth/', include('social_django.urls', namespace='social')),


]
