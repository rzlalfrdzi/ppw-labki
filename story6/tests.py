from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from selenium import webdriver
import unittest
import time
from .views import *
from .forms import forms
from .models import Status

class Lab6IndexTest(TestCase):
    def test_index_url_is_exist(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code,200)

    def test_index_using_index_template(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'index2.html')

    def test_index_using_index_func(self):
        found = resolve('/story6/')
        self.assertEqual(found.func, index)

    def test_model_can_add_new_status(self):
        Status.objects.create(forms="test")
        count_all_status = Status.objects.all().count()
        self.assertEqual(count_all_status, 1)
    
    def test_form_valid(self):
        form_data = {"status": "test"}
        form = forms(data=form_data)
        self.assertTrue(form.is_valid())
    
    def test_addstatus_request(self):
        response = Client().post('/addstatus/', {"status": "test"})
        response = Client().get('/story6/')
        self.assertTrue(response, "test")

    from selenium import webdriver


class InputNewText(LiveServerTestCase):
    def SetUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25) 
        super(InputNewText,self).setUp()


    def tearDown(self):
        self.selenium.quit()
        super(InputNewText, self).tearDown()


    def test_input(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000')
        search_box = selenium.find_element_by_id('id_status')
        time.sleep(5)
        submit_button = selenium.find_element_by_id("submitbtn")
        search_box.send_keys('foo bar')
        time.sleep(3)
        submit_button.send_keys(Keys.RETURN)

    def test_button_size(self):
    	selenium = self.selenium
    	selenium.get(self.live_server_url)
    	tombol = selenium.find_element_by_id('submitbtn')
    	tombol_size = tombol.size
    	#print(tombol_size)
        self.assertEqual(tombol_size, {'height': 42, 'width': 54})

    def test_button_location(self):
    	selenium = self.selenium
    	selenium.get(self.live_server_url)
    	button1 = selenium.find_element_by_id('submitbtn')
    	button1_location = button1.location
    	self.assertEqual(button1_location, {'x': 2, 'y': 446})

    def test_text_header_css(self):
    	selenium = self.selenium
    	selenium.get(self.live_server_url)
    	teks = selenium.find_element_by_id('textheader')
    	ukuran = teks.value_of_css_property('font-size')
    	self.assertEqual(ukuran, '14px')

    def test_button_css(self):
    	selenium = self.selenium
    	selenium.get(self.live_server_url)
    	tombol2 = selenium.find_element_by_id('submitbtn')
    	warna = tombol2.value_of_css_property('background-color')
    	border_radius = tombol2.value_of_css_property('border-radius')
    	self.assertEqual(warna, 'rgb(128, 128, 128)')
    	self.assertEqual(border_radius, '4px')




    	
