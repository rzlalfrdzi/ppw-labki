from django.conf.urls import url
from django.conf.urls import url
from .views import index, addstatus, delete, myprofile

appname = 'story6'


urlpatterns = [
    url(r'^$', index, name='index'),
    url('add_status',addstatus,name="add_status"),
    url(r'delete', delete, name="delete"),
    url('myprofile',myprofile,name="myprofile")
]
