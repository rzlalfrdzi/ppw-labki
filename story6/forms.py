from django import forms

class forms(forms.Form):
    status_attrs = {
        "type" : "text",
        "placeholder": "hello, tell us what you feel",
        "class":"search-text",
        "name":"newstatus"
    }
    status = forms.CharField(max_length=200, required=True , widget=forms.TextInput(attrs=status_attrs))