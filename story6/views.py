from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse

from .forms import forms
from .models import Status
# Create your views here.

def index(request):
    response = {
        'status':forms,
        'events': Status.objects.all()
    }
    return render(request, "index2.html",response)

def addstatus(request):     #pragma no cover
    form = forms(request.POST)

    if(request.method == "POST"):
        if(form.is_valid()):
            clean = form.cleaned_data
            status = Status()
            status.forms = clean['status']
            status.save()
            print(status)
            return HttpResponseRedirect('/story6')
        else:
            return HttpResponse("hello")
    
    else:
        return(request, 'index2.html')

def delete(request):        #pragma no cover
    item = get_object_or_404(item, pk=item_id)
    item.delete()
    return HttpResponseRedirect('/story6')

def myprofile(request):
    return HttpResponseRedirect('profile.html')